
<#PSScriptInfo

.VERSION 1.0

.GUID 5d95c56b-77b5-4ed6-bdb0-b92a36925d78

.AUTHOR Alexander Perepechko <aperepechko@protonmail.com>

.LICENSEURI https://gitlab.com/perepechko.alex/touchps/-/raw/master/LICENSE

.PROJECTURI https://gitlab.com/perepechko.alex/touchps

.RELEASENOTES
Initial Commit

#>

<# 

.DESCRIPTION 
 A PowerShell equivalent to the *nix touch command that allows for the creation of empty files 

#> 



function New-File {
    $file = $args[0]
    if ($null -eq $file) {
        throw "No filename argument provided"
    }
    if (Test-Path $file) {
        (Get-ChildItem $file).LastWriteTime = Get-Date
        Write-Host  "Created $file"
    } else {
        New-Item $file
    }
}