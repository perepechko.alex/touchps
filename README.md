This script provides a PowerShell equivalent to the *nix `touch` command. While PowerShell does contain the `New-File` cmdlet, it does not override files and modify their timestamp. This is also simpler to use and works much like the traditional *nix command.

## Setup
1. Download `NewFile.ps1` to the desired location.
2. In your `Microsoft.PowerShell_profile.ps1` file, add the following lines:
   ```
   Import-Module path\to\NewFile.ps1
   Set-Alias -Name touch -Value New-File
   ```

## How to Use
To utilize the command, assuming the alias is set to `touch`, run the following:

```
touch fileName.ext
```
